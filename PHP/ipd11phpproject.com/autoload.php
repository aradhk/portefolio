<?php

spl_autoload_register(function($a){
    
    if(class_exists($a, false))
    {
        return;
    }
    
    $file = __DIR__ . '/' . str_replace('\\', '/', $a) . '.php';
    
    if($file && file_exists($file))
    {
        include $file;
    }
});
