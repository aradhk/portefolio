<?php

namespace Database;

class DbSettings
{
    private $settings;
    
    public function __construct($driver, $host, $port, $dbname, $username, $password, array $options=null)
    {
        $this->settings = array(
            'driver' => $driver,
            'host' => $host,
			'port' => $port,
			'dbname' => $dbname,
            'username' => $username,
            'password' => $password,
            'options' => $options
        );
		
    }
    
    public function getSettings() {
        return $this->settings;
    }
}