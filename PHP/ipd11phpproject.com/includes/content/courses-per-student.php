<h1>Students by course</h1>
<div id="status-message"></div>
<form class="form-inline" id="studentbycourse-form" action="/api/student/by/course" method="POST">
    <div class="row">
        <div class="form-group mx-sm-3 mb-2">
            <label for="code" class="sr-only">Course code</label>
            <input type="text" class="form-control" name="code" id="code" placeholder="Course code" required>
        </div>
    </div>
    <button  id="btnCodeSubmit" type="submit" class="btn btn-primary mb-2">Go</button>
</form>
<table class="table table-bordered" >
    <thead>
        <tr>
            <th>Code & # of students</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Date of birth</th>
        </tr>
    </thead>
	<tbody id="studentbycousetbody"></tbody>
</table>