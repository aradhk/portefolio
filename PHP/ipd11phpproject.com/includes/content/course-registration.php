<h1>Course Registration</h1>
<form id="course-reg-form" action="/api/course/addstudent" method="post">
<div id="status-message3"></div>
    <div class="form-group" >
	
        <label for="studentid">Student</label>
		<div id="status-message"></div>
        <select class="form-control" name="studentId" id="studentDDListid" aria-describedby="studentidHelp" Required >
            <option value="">Select student</option>
        </select>
        <small id="studentidHelp" class="form-text text-muted">List of students.</small>
    </div>
    <div class="form-group">
        <label for="courseid">Course</label>
		<div id="status-message1"></div>
        <select class="form-control" name="courseId" id="courseDDListid" aria-describedby="courseidHelp" Required>
            <option value="">Select course</option>
        </select>
        <small id="courseidHelp" class="form-text text-muted">List of courses.</small>
    </div>
    <button type="submit" class="btn btn-primary">Register</button>
</form>