<h1>List of Courses</h1>
<div id="status-message"></div>
<table id="courseTable" class="table table-bordered">
    <thead>
        <tr>
            <th>Code</th>
            <th>Name</th>
            <th>Description</th>
            <th># of students</th>
			<th>Action</th>
        </tr>
    </thead>
	<tbody></tbody>
</table>

<form id="course-form" action="/api/course/add" method="post">
    <h1>Add Course</h1>
	<div id="status-message1"></div>
    <div class="form-group">
        <label for="code">Code</label>
        <input type="text" class="form-control" name="code" id="code" aria-describedby="codeHelp" placeholder="Course code" required>
        <small id="codeHelp" class="form-text text-muted">Course code.</small>
    </div>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Course name" required>
        <small id="nameHelp" class="form-text text-muted">Course name.</small>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" name="description" id="description" aria-describedby="descriptionHelp" placeholder="Course description" required></textarea>
        <small id="descriptionHelp" class="form-text text-muted">Course description.</small>
    </div>
    <button type="submit" class="btn btn-primary">Add</button>
</form>