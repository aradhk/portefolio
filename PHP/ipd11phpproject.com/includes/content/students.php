<h1>List of Students</h1>
<div id="status-message"></div>
<table id="studentTable" class="table table-bordered" >
    <thead>
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Date of birth</th>
			<th>Action</th>
        </tr>
    </thead>
	<tbody></tbody>
</table>

<form id="student-form" action="/api/student/add" method="POST">
    <h1>Add Student</h1>
	<div id="status-message1"></div>
    <div class="form-group">
        <label for="first_name">First name</label>
        <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="first_nameHelp" placeholder="First name" required >
        <small id="first_nameHelp" class="form-text text-muted">First name.</small>
    </div>
    <div class="form-group">
        <label for="last_name">Last name</label>
        <input type="text" class="form-control" name="last_name" id="last_name" aria-describedby="last_nameHelp" placeholder="Last name" required >
        <small id="last_nameHelp" class="form-text text-muted">Last name.</small>
    </div>
    <div class="form-group">
        <label for="dob">DoB</label>
        <input type="date" class="form-control" name="dob" id="dob" aria-describedby="dobHelp" placeholder="Date of birth" required >
        <small id="dobHelp" class="form-text text-muted">Date of birth.</small>
    </div>
    <button type="submit" class="btn btn-primary">Add</button>
</form>