/**
 * JS function for handling form submission
 * 
 * @param {type} formElement
 * @returns {undefined}
 */
 
 
 //import { Spinner } from '/assets/js/spin.js';
 
 var opts = {
  lines: 13, // The number of lines to draw
  length: 38, // The length of each line
  width: 17, // The line thickness
  radius: 45, // The radius of the inner circle
  scale: 1, // Scales overall size of the spinner
  corners: 1, // Corner roundness (0..1)
  color: '#000', // CSS color or array of colors
  fadeColor: 'transparent', // CSS color or array of colors
  opacity: 0.25, // Opacity of the lines
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  fps: 20, // Frames per second when using setTimeout() as a fallback in IE 9
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  className: 'spinner', // The CSS class to assign to the spinner
  top: '50%', // Top position relative to parent
  left: '50%', // Left position relative to parent
  shadow: 'none', // Box-shadow for the lines
  position: 'absolute' // Element positioning
};



  
 
function studentFormSubmissionHandler(formElement)
{
    var statusMessageContainer = $('#status-message1');
	statusMessageContainer.html('');
    
    formElement.submit(function(evt){
        
        var thisForm = $(this);
        var data = thisForm.serialize();
        var spinner = new Spinner(opts).spin(document.getElementById('student-form'));
        jQuery.ajax({
            url: thisForm.attr('action'),
            type: thisForm.attr('method'),
            data: data,
            dataType: 'json',
            error: function(a, b, c) {
                console.error('Error while submitting form: ' + b);
            },
            success: function(response) {
                // reset form
                thisForm[0].reset();
				
                // display output
                if (response.error)
                {
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
					
                    statusMessageContainer.html(alertBox('success', response.message));
					
					updateStudentTable();
                }
                
                statusMessageContainer.find('.alert').alert();
            }
        });
        setTimeout( function() {spinner.stop();},300);
        evt.preventDefault();
        return false;
    });
	
	
}


function courseFormSubmissionHandler(FormElement){
	
	var statusMessageContainer = $('#status-message2');
	
	FormElement.submit(function(evt){
		
		var thisForm = $(this);
        var data = thisForm.serialize();
		var spinner = new Spinner(opts).spin(document.getElementById('course-form'));
		jQuery.ajax({
			url: thisForm.attr('action'),
			type: thisForm.attr('method'),
			data: data,
			dataType: 'json',
			error: function(jqXHR, textStatus, errorThrown){
				console.error('Error while submitting form: ' + errorThrown + textStatus) ;	
				statusMessageContainer.html(alertBox('danger', ": Error occured while trying to subscribe. Please try again later: "+errorThrown ));				
			},
			success: function(response){
				thisForm[0].reset();
                // display output
                if (response.error)
                {
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
                    statusMessageContainer.html(alertBox('success', response.message));
					updateCourseTable();
                }
                
                statusMessageContainer.find('.alert').alert();
				
			}
			
			
		});
		setTimeout( function() {spinner.stop();},300);
		evt.preventDefault();
        return false;
	});
	
}



function listStudentHandler(studentTableElement){
	
	var statusMessageContainer = $('#status-message');
	statusMessageContainer.html('');
	//var studentTableElement= $this;
	
	var action= '/api/student/listing';
	var method= 'post';
	jQuery.ajax({
			url: action,
			type: method,			
			dataType: 'json',
			error: function(jqXHR, textStatus, errorThrown){
				console.error('Error while submitting form: ' + errorThrown);	
							
			},
			success: function(response){
				
                // display output
                if (response.error)
                {
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
                    //subscriberTableElement.append("<tr><td>" + data.column1 + "</td><td>" + data.column2 + "</td><td>" + data.column3 + "</td></tr>");
					
					
					populateStudentTable(response.data, studentTableElement.attr('id'));
					
					
        
                }
                
                statusMessageContainer.find('.alert').alert();
				
			}
			
			
		});
	
	
}


 function populateStudentTable(data, tableId){
	var htmlContent=[];
	
	if(data.length && data){
		var i;
		
		for(i=0; i<data.length ; i++){
			
			htmlContent.push('<tr><td>');
			htmlContent.push(data[i]['first_name']);
			htmlContent.push('</td><td>');
			htmlContent.push(data[i]['last_name']);
			htmlContent.push('</td><td>');
			htmlContent.push(data[i]['dob']);
			htmlContent.push('</td><td>');
			htmlContent.push('<a href="javascript:void(0);" onclick="deleteStudent('+data[i].id+')"><i class="fa fa-remove"></i></a>');
			htmlContent.push('</td></tr>');
			
		}
		tableId ='#'+ tableId + ' tbody';
		$(tableId).html(htmlContent.join(''));
		
		
	}else if(data.length ==0){
		tableId ='#'+ tableId + ' tbody';
		$(tableId).html('');
	}
	
}



function listCourseHandler(TableElement){
	
	var statusMessageContainer = $('#status-message');
	statusMessageContainer.html('');
	
	var action= '/api/course/listing';
	var method= 'post';
	jQuery.ajax({
			url: action,
			type: method,			
			dataType: 'json',
			error: function(jqXHR, textStatus, errorThrown){
				console.error('Error while submitting form: ' + errorThrown);	
							
			},
			success: function(response){
				
                // display output
                if (response.error)
                {
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
                    //subscriberTableElement.append("<tr><td>" + data.column1 + "</td><td>" + data.column2 + "</td><td>" + data.column3 + "</td></tr>");
					
					populateCourseTable(response.data, TableElement.attr('id'));
        
                }
                
                statusMessageContainer.find('.alert').alert();
				
			}
			
			
		});
	
	
}



 function populateCourseTable(data, tableId){
	var htmlContent=[];
	
	if(data.length && data){
		var i;
		for(i=0; i<data.length ; i++){
			
			htmlContent.push('<tr><td>');
			htmlContent.push(data[i]['code']);
			htmlContent.push('</td><td>');
			htmlContent.push(data[i]['name']);
			htmlContent.push('</td><td>');
			htmlContent.push(data[i]['description']);
			htmlContent.push('</td><td>');
			htmlContent.push(data[i]['noOfStudents']);
			htmlContent.push('</td><td>');
			htmlContent.push('<a href="javascript:void(0);" onclick="deleteCourse('+data[i].id+')"><i class="fa fa-remove"></i></a>');
			htmlContent.push('</td></tr>');
			
		}
		tableId ='#'+ tableId + ' tbody';
		$(tableId).html(htmlContent.join(''));
				
	}else if(data.length ==0){
		tableId ='#'+ tableId + ' tbody';
		$(tableId).html('');
	}
	
}


function studentDDlistHandler(dDlist){
	
	var statusMessageContainer = $('#status-message');
	//statusMessageContainer.html('');
	
	//$(dDlist).insertBefore(new Option('', ''),$(dDlist).firstChild);
	var action= '/api/student/listing';
	var method= 'post';
	jQuery.ajax({
			url: action,
			type: method,			
			dataType: 'json',
			error: function(jqXHR, textStatus, errorThrown){
				console.error('Error while submitting form: ' + errorThrown);	
							
			},
			success: function(response){
				
                // display output
                if (response.error)
                {
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
						
						$.each(response.data,function (index, item) {
							$(dDlist).append('<option value="'+ item.id +'">'+'Student ID: ['+item.id+']  Name: '+ item.first_name + ' ' +item.last_name+'</option>');
							});
                }                
                statusMessageContainer.find('.alert').alert();				
			}					
		});	
}
function courseDDlistHandler(dDlist){
	
	var statusMessageContainer = $('#status-message2');
	//statusMessageContainer.html('');
	
	//$(dDlist).insertBefore(new Option('', ''),$(dDlist).firstChild);
	var action= '/api/course/listing';
	var method= 'post';
	jQuery.ajax({
			url: action,
			type: method,			
			dataType: 'json',
			error: function(jqXHR, textStatus, errorThrown){
				console.error('Error while submitting form: ' + errorThrown);	
							
			},
			success: function(response){
				
                // display output
                if (response.error)
                {
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
						
						$.each(response.data,function (index, item) {
							$(dDlist).append('<option value="'+ item.id +'">'+'Course code: ['+item.code+']   Name: '+ item.name + ' </option>');
							});
                }                
                statusMessageContainer.find('.alert').alert();				
			}					
		});	
}


function courseRegFormHandler(formElement){
	
	var statusMessageContainer = $('#status-message3');
	statusMessageContainer.html('');    
    formElement.submit(function(evt){        
        var thisForm = $(this);
        var data = thisForm.serialize();
        
        jQuery.ajax({
            url: thisForm.attr('action'),
            type: thisForm.attr('method'),
            data: data,
            dataType: 'json',
            error: function(a, b, c) {
                console.error('Error while submitting form: ' + b);
            },
            success: function(response) {
                // reset form
                thisForm[0].reset();
				
                // display output
                if (response.error)
                {
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
					
                    statusMessageContainer.html(alertBox('success', response.message));
					
				}
                
                statusMessageContainer.find('.alert').alert();
            }
        });
        
        evt.preventDefault();
        return false;
    });
	
}

function studentbycourseFormHandler(formElement){
	var statusMessageContainer = $('#status-message');
	statusMessageContainer.html(''); 
	
	
    formElement.submit(function(evt){        
        var thisForm = $(this);
        var data = thisForm.serialize();
		var spinner = new Spinner(opts).spin(document.getElementById('studentbycourse-form'));
        
		 
        jQuery.ajax({
            url: thisForm.attr('action'),
            type: thisForm.attr('method'),
            data: data,
            dataType: 'json',
            error: function(a, b, c) {
                console.error('Error while submitting form: ' + b);
            },
            success: function(response) {
                // reset form
                
				$("#studentbycousetbody").empty();
			
                // display output
                if (response.error)
                {
					thisForm[0].reset();
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
					
					
					if(!((response.data).length>0)){
						
						$("#studentbycousetbody").append('<tr><td> <b>Sorry, No course matches the code ['+$('#code').val()+'] entered! Or no students registered yet! <b></td></tr>');
							
					}
											
					
						$.each(response.data, function (index, item){

							if(item.noOFstudent!=0){
								if(index==0){
									$("#studentbycousetbody").append('<tr><td> Code:'+item.code+', Course name: '+item.name+', No of students ='+item.noOFstudent+'</td><td>'+item.first_name+'</td><td>'+item.last_name+'</td><td>'+item.dob+'</td></tr>');
									
								}else{
									
									$("#studentbycousetbody").append('<tr><td></td><td>'+item.first_name+'</td><td>'+item.last_name+'</td><td>'+item.dob+'</td></tr>');
									
								}
								
								
							}else{
								$("#studentbycousetbody").append('<tr><td> <b>Sorry, No course matches the code ['+$('#code').val()+'] entered! Or no students registered yet! <b></td></tr>');
							}
						});
						thisForm[0].reset();
                }                
                statusMessageContainer.find('.alert').alert();				
			}	
				
		});	
		setTimeout( function() {spinner.stop();},300);
	 
        evt.preventDefault();
        return false;
    });
	
}



/**
 * 
 * JS function for wrapping alert message
 * 
 * @param {String} type
 * @param {String} message
 * @returns {String}
 */
function alertBox(type, message)
{
    var html = [];
    html.push('<div class="alert alert-'+(type||'info')+' alert-dismissible fade show" role="alert">');
    html.push('<button type="button" class="close" data-dismiss="alert" aria-label="Close">');
    html.push('<span aria-hidden="true">&times;</span>');
    html.push('</button>');
    html.push('<div class="message">'+message+'</div>');
    html.push('</div>');
    return html.join('');
}

function deleteStudent(id)
{
	var statusMessageContainer = $('#status-message1');
	statusMessageContainer.html('');
	
	var action= '/api/student/deletestudent';
	var method= 'DELETE';
	
	var spinner = new Spinner(opts).spin(document.getElementById('student-form'));
		jQuery.ajax({
			url: action,
			type: method,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("studentid", id);
			},
			dataType: 'json',
			error: function(jqXHR, textStatus, errorThrown){
				console.error('Error while submitting form: ' + errorThrown + textStatus) ;	
				statusMessageContainer.html(alertBox('danger', ": Error occured while trying to subscribe. Please try again later: "+errorThrown ));				
			},
			success: function(response){
				// display output
                if (response.error)
                {
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
                    statusMessageContainer.html(alertBox('success', response.message));
					updateStudentTable();
                }
                
                statusMessageContainer.find('.alert').alert();
				
			}
			
			
		});
		setTimeout( function() {spinner.stop();},300);
}

function deleteCourse(id)
{
	var statusMessageContainer = $('#status-message1');
	statusMessageContainer.html('');
	
	var action= '/api/course/deletecourse';
	var method= 'DELETE';
	
	var spinner = new Spinner(opts).spin(document.getElementById('course-form'));
		jQuery.ajax({
			url: action,
			type: method,
			beforeSend: function(xhr) {
				xhr.setRequestHeader("courseid", id);
			},
			dataType: 'json',
			error: function(jqXHR, textStatus, errorThrown){
				console.error('Error while submitting form: ' + errorThrown + textStatus) ;	
				statusMessageContainer.html(alertBox('danger', ": Error occured while trying to subscribe. Please try again later: "+errorThrown ));				
			},
			success: function(response){
				// display output
                if (response.error)
                {
                    statusMessageContainer.html(alertBox('danger', response.error));
                }
                else if (response.success)
                {
                    statusMessageContainer.html(alertBox('success', response.message));
					updateCourseTable();
                }
                
                statusMessageContainer.find('.alert').alert();
				
			}
			
			
		});
		setTimeout( function() {spinner.stop();},300);
}

/**
 * After HTML Document loaded
 */
$(document).ready(function(){
    
    var studentFormElement = $('#student-form');
	var courseFormElement = $('#course-form');
	var studentDDlist=$('#studentDDListid');
	var courseDDlist=$('#courseDDListid');	
	var courseRegForm =$('#course-reg-form');
	var studentbycourseForm=$('#studentbycourse-form')
	updateStudentTable();
	updateCourseTable();
    
    // If the contact form exists
    if (studentFormElement.length)
    {
        // Triger contact form submission handler
        studentFormSubmissionHandler(studentFormElement);
    }
	if (courseFormElement.length)
    {
        // Triger course form submission handler
        courseFormSubmissionHandler(courseFormElement);
    }
	if(studentDDlist.length){
		
		studentDDlistHandler(studentDDlist);
	}
	if(courseDDlist.length){
		
		courseDDlistHandler(courseDDlist);
	}
	if(courseRegForm.length){
		
		courseRegFormHandler(courseRegForm);
	}
	if(studentbycourseForm.length){
		studentbycourseFormHandler(studentbycourseForm);
	}
	
});

function updateStudentTable() {
	
	var studentTableElement = $('#studentTable');
	
	if(studentTableElement.length){
		
		listStudentHandler(studentTableElement);
		
	}
}

function updateCourseTable() {
	
	var courseTableElement = $('#courseTable');
	
	if(courseTableElement.length){
		
		listCourseHandler(courseTableElement);
	}
}