<?php

namespace ProjectApp\Services;

class Course extends \ProjectApp\ContextProcessorServiceAbstract
{
    private $uriParts = array();
    
    public function setUriParts(array $uriParts)
    {
        $this->uriParts = $uriParts;
    }
    
    public function execute()
    {
        // check if 
        if (sizeof($this->uriParts) && $this->uriParts[0])
        {
            if (method_exists($this, $this->uriParts[0]))
            {
                $this->{$this->uriParts[0]}();
            }
            else
            {
                $this->output = array('error' => 'Method '. $this->uriParts[0]);
            }
        }
        else
        {
            $this->output = array('error' => 'Illegal request.');
        }
    }
    
    private function add()
    {
        // process the data (i.e. save to database or/and send email)
		$dbo = $this->getDbo();		
		$statement = "INSERT INTO courses (code, name, description) VALUES ('".$_POST["code"]."','".$_POST["name"]."','".$_POST["description"]."')";
		$dbo->query($statement);
			
		$dbo = null;  
		//die($statement);
        $this->output = array(
            'success' => true,
            'message' => $_POST["name"].' was successfully added!'
        );
		
		
    }
	
	private function listing()
    {
		$dbo = $this->getDbo();		
		$statement1 = "SELECT courses.id, courses.code, courses.name, courses.description , COUNT(student_courses.student_id) as noOfStudents FROM courses LEFT JOIN student_courses ON courses.id=student_courses.course_id GROUP BY courses.id, courses.code, courses.name, courses.description";
		
		$list=$dbo->loadAssocList($statement1);
		
		
		 $this->output = array(
            'success' => true,
            'data' => $list
			
			
        );
	}
	
	private function addstudent(){
		
		$dbo = $this->getDbo();		
		$statement = "INSERT INTO student_courses (student_id, course_id) VALUES ('".$_POST["studentId"]."','".$_POST["courseId"]."')";
		
		
		
		$dbo->query($statement);
			
		$dbo = null;  
		//die($statement);
        $this->output = array(
            'success' => true,
            'message' => 'Student with Id= '.$_POST["studentId"].' was successfully added the course with Id=  '.$_POST["courseId"]
        );
		
		
	}
	
	private function deletecourse(){
		
		if (strtoupper($_SERVER['REQUEST_METHOD']) === 'DELETE') {
			
			$dbo = $this->getDbo();		
			$statement1 = "DELETE FROM student_courses where student_id=".$this->getHeader('courseid', 0);
			$statement2 = "DELETE FROM courses where id=".$this->getHeader('courseid', 0);
			
			//die(json_encode(array($statement1.' || '.$statement2)));
					
			//die($statement1);
			
			$dbo->query($statement1);
			$dbo->query($statement2);
			
			//print_r($list);
			//die();
			
			$this->output = array(
				'success' => true,
				'message' => "The student with id = ".$this->getHeader('courseid', 0)." was successfully deleted"
			);
		}
		else
		{
			$this->output = array(
				'error' => "You are trying to delete record illegally!"
			);
		}
	}
}