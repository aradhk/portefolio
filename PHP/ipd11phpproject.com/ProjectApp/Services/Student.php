<?php

namespace ProjectApp\Services;

class Student extends \ProjectApp\ContextProcessorServiceAbstract
{
    private $uriParts = array();
	
    
    public function setUriParts(array $uriParts)
    {
        $this->uriParts = $uriParts;
    }
    
    public function execute()
    {
        // check if 
        if (sizeof($this->uriParts) && $this->uriParts[0])
        {
			if(sizeof($this->uriParts) ==1){
				
					if (method_exists($this, $this->uriParts[0]))
				{
					$this->{$this->uriParts[0]}();
				}
				else
				{
					$this->output = array('error' => 'Method '. $this->uriParts[0]);
				}
				
				
			}else 
			{
				//print_r($this->uriParts);
				
				  $method = implode('', $this->uriParts);
				 
				 //die($method);
				 if (method_exists($this, $method))
				{
					$this->{$method}();
				}
				else
				{
					$this->output = array('error' => 'Method '. $method);
				}
				 
			}
			
            
        }
        else
        {
            $this->output = array('error' => 'Illegal request.');
        }
    }
    
    private function add()
    {
        // process the data (i.e. save to database or/and send email)
        $dbo = $this->getDbo();		
	
		$statement = "INSERT INTO students (first_name, last_name, dob) VALUES ('".$_POST["first_name"]."','".$_POST["last_name"]."','".$_POST["dob"]."')";

		$dbo->query($statement);
			
		$dbo = null;  
		//die($statement);
        $this->output = array(
            'success' => true,
            'message' => 'The student '.$_POST["first_name"]." ".$_POST["last_name"].' was added successfully :)'
        );
		
		
    }
	
	private function listing()
    {
		$dbo = $this->getDbo();				
		$statement1 = "SELECT * FROM students";
		
		$list=$dbo->loadAssocList($statement1);
		
		$dbo = null;  
		 $this->output = array(
            'success' => true,
            'data' => $list
			
			
        );
	}
	
	private function bycourse(){
		
		$dbo = $this->getDbo();		
		$statement1 = "SELECT courses.code, courses.name, (select count(student_id) from student_courses where course_id= courses.id)  as noOFstudent, students.first_name, students.last_name, students.dob  FROM students join student_courses on students.id = student_courses.student_id join courses on courses.id = student_courses.course_id  where courses.code=".$_POST['code'];
				
		//die($statement1);
		
		$list=$dbo->loadAssocList($statement1);
		
		//print_r($list);
		//die();
		$dbo = null;  
		 $this->output = array(
            'success' => true,
            'data' => $list
			
        );
	}
	
	private function deletestudent(){
		
		if (strtoupper($_SERVER['REQUEST_METHOD']) === 'DELETE') {
			
			$dbo = $this->getDbo();		
			$statement1 = "DELETE FROM student_courses where student_id=".$this->getHeader('studentid', 0);
			$statement2 = "DELETE FROM students where id=".$this->getHeader('studentid', 0);
			
			//die(json_encode(array($statement1.' || '.$statement2)));
					
			//die($statement1);
			
			$dbo->query($statement1);
			$dbo->query($statement2);
			$dbo = null;  
			//print_r($list);
			//die();
			
			
			$this->output = array(
				'success' => true,
				'message' => "The student with id = ".$this->getHeader('studentid', 0)." was successfully deleted"
			);
		}
		else
		{
			$this->output = array(
				'error' => "You are trying to delete record illegally!"
			);
		}
	}
}