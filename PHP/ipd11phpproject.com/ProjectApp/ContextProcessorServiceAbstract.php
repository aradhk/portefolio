<?php

namespace ProjectApp;

abstract class ContextProcessorServiceAbstract
{
	private $dbConfig = array(
	
		'config1' => array('mysql','localhost',3306,'cp4795_Ahmed','cp4795_Ahmed','AhmedArad'),
		'config2' => array('mysql','localhost',3306,'ipd11phpproject','phpuser','PhpUserPasswd')
		
	);
	private static $dbo = null;
	private static $headers = null;
    protected $output = array();

    abstract public function setUriParts(array $uriParts);
    
    abstract public function execute();
    
    public final function getOutputAsArray()
    {
        return $this->output;
    }
	
	public final function getDbo($config='config2')
	{
		if (self::$dbo === null && isset($this->dbConfig[$config]) && sizeof($this->dbConfig[$config])===6)
		{
			$configData = $this->dbConfig[$config];
			$dbSettings = new \Database\DbSettings($configData[0], $configData[1], $configData[2], $configData[3], $configData[4], $configData[5]);
			self::$dbo = new \Database\Dbo($dbSettings);
		}
		
		return self::$dbo;
	}
	
	public final function getHeader($param, $default=null)
	{
		if (self::$headers === null)
		{
			self::$headers = getallheaders();
		}
		
		if (is_array(self::$headers) && isset(self::$headers[$param]))
		{
			return self::$headers[$param];
		}
		
		return $default;
	}
}