<?php

// set response header to be JSON type
// with character set UTF-8
header('Content-Type: application/json; charset=UTF-8');

// param uri passed by .htaccess
$uri = isset($_GET['uri']) ? $_GET['uri'] : '';

// if uri exists
if ($uri)
{
	include __DIR__ . '/Lib/autoload.php';
    include __DIR__ . '/autoload.php';
    $cp = new ProjectApp\ContextProcessor();
    $cp->process($uri);
    echo json_encode($cp->getOutputAsArray());
}
else
{
    echo json_encode(array('error'=>'Illegal request'));
}