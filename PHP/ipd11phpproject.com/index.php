<?php /*

run window with Win+R
sysdm.cpl
advanced tab
In the bottom environment variables
in System path section,
Selectn "Path"
Click on Edit
Click on new
Add your php xamp path (C:\xampp\php)

open command prompt:
    open RUN window (win + R)
    type CMD
    press enter
    in the command prompte type:
        php -v



*/ ?>
<?php include __DIR__ . '/defines.php'; ?>
<html>
    <head>
		<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/assets/css/style.css" />
        <title>IPD11 - PHP - Project</title>
    </head>
    <body>
        <div class="container">
            <?php include INCLUDES_DIR . '/header.php'; ?>
            <main role="main">
                <?php include Lib\Request::contentFile(); ?>
            </main>
            <?php include INCLUDES_DIR . '/footer.php';?>
        </div>
        <script src="/assets/bootstrap/js/jquery-v3.2.1.min.js"></script>
        <script src="/assets/bootstrap/js/popper.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap-v4.0.0-beta.3.min.js"></script>
        <script src="/assets/js/spin.js"></script>
		<script src="/assets/js/script.js"></script>
    </body>
</html>