use MovinOn_Db;
/**** add foreign key constraint(s) to the  tblUnitRental ****/
/**** fk_tblunitrental_tblcustomer ****/

alter table tblunitrental
	add constraint fk_tblunitrental_tblcustomer foreign key(CustID )
    references tblcustomer(CustID);
    
/**** add foreign key constraint(s) to the  tblUnitRental ****/
/**** fk_tblunitrental_tblwarehouse ****/

alter table tblunitrental
	add constraint fk_tblunitrental_tblwarehouse foreign key(WarehouseID)
    references tblwarehouse(WarehouseID);
    
/**** add foreign key constraint(s) to the  tbljoborder ****/
/**** fk_tbljoborder_tblcustomer ****/

alter table tbljoborder
	add constraint fk_tbljoborder_tblcustomer foreign key(CustID)
    references tblcustomer(CustID);

/**** add foreign key constraint(s) to the  tbljobdetail ****/
/**** fk_tbljobdetail_tblvehicle ****/

alter table tbljobdetail
	add constraint fk_tbljobdetail_tblvehicle foreign key(VehicleID)
    references tblvehicle(VehicleID);
    
/**** add foreign key constraint(s) to the  tbljobdetail ****/
/**** fk_tbljobdetail_tbldriver ****/

alter table tbljobdetail
	add constraint fk_tbljobdetail_tbldriver foreign key(DriverID)
    references tbldriver(DriverID);
/**** add foreign key constraint(s) to the  tbljobdetail ****/
/**** fk_tbljobdetail_tbljoborder ****/
alter table tbljobdetail
	add constraint fk_tbljobdetail_tbljoborder foreign key(JobID)
    references tbljoborder(JobID);
    
/**** add foreign key constraint(s) to the  tbljobdetail ****/
/**** fk_tbljobdetail_tbljoborder ****/

-- alter table tbljobdetail
	-- add constraint fk_tbljobdetail_tbljoborder foreign key(JobID)
   --  references tbljoborder(JobID);
-- alter table tbljobdetail
	-- drop foreign key fk_tbljobdetail_tbljoborder;

    
/**** add foreign key constraint(s) to the  tblstorageunit ****/
/**** fk_tblstorageunit_tblwarehouse ****/

alter table tblstorageunit
	add constraint fk_tblstorageunit_tblwarehouse foreign key(WarehouseID)
    references tblwarehouse(WarehouseID);

/**** add foreign key constraint(s) to the  tblemployee ****/
/**** fk_tblemployee_tblwarehouse ****/

alter table tblemployee
	add constraint fk_tblemployee_tblwarehouse foreign key(WarehouseID)
    references tblwarehouse(WarehouseID);
    
    
/**** add foreign key constraint(s) to the  tblemployee ****/
/**** fk_tblemployee_tblposition ****/

alter table tblemployee
	add constraint fk_tblemployee_tblposition foreign key(PositionID)
    references tblposition(PositionID);
    
/**** add foreign key constraint(s) to the  tblemployee ****/
/**** fk_tblemployee_tblposition ****/