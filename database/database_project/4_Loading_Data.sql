use MovinOn_Db;

/***** Loading Data to tblcustomer*****/
/***** 1 *****/
load data local infile 'C:/Users/Admin/Customer.csv'
into table tblcustomer
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblcustomer;

/***** Loading Data to tblposition*****/
/***** 2 *****/
load data local infile 'C:/Users/Admin/JobPositionData.csv'
into table tblposition
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblposition;

/***** Loading Data to tblwarehouse*****/
/***** 3 *****/
load data local infile 'C:/Users/Admin/WarehousesData.csv'
into table tblwarehouse
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblwarehouse;


/***** Loading Data to tbldriver*****/
/***** 4 *****/
load data local infile 'C:/Users/Admin/DriversData.csv'
into table tbldriver
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines
;


select *
from tbldriver;

/***** Loading Data to tblvehicle*****/
/***** 5 *****/
load data local infile 'C:/Users/Admin/VehiclesData.csv'
into table tblvehicle
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblvehicle;

/***** Loading Data to tblUnitRentalsData*****/
/*****6 *****/

load data local infile 'C:/Users/Admin/UnitRentalsData1.csv'
into table tblunitrental
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines
;


select *
from tblunitrental;

/***** Loading Data to tblstorageunit*****/
/***** 7 *****/

load data local infile 'C:/Users/Admin/StorageUnitsData7.csv'
into table tblstorageunit
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblstorageunit;

/***** Loading Data to tblemployee*****/
/***** 8 *****/
load data local infile 'C:/Users/Admin/EmployeesData.csv'
into table tblemployee
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblemployee;

/***** Loading Data to tbljoborder*****/
/***** 9 *****/
load data local infile 'C:/Users/Admin/JobOrdersData.csv'
into table tbljoborder
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

-- delete from tbljoborder
	-- where JobID between 2 and 29;

 
select *
from tbljoborder;


/***** Loading Data to tbljobdetail*****/
/***** 10 *****/


load data local infile 'C:/Users/Admin/JobDetails.csv'
into table tbljobdetail
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines 
;

select *
from tbljobdetail;
