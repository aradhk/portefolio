/*Movin_AradAhmed_creating Tables
Script Data:august  2017
Developed by:Arad & Ahmed
*/
/*switch to the data base*/
use MovinOn_AradAhmed;

/***** create Customer Table *****/
/****** 1 ******/
drop table  if exists tblCustomer ; 
create table tblCustomer(
	CustID int auto_increment  not null,
    CompanyName varchar(40)  null,
    ContactFirst varchar(40) not null,
    ContactLast varchar(40) not null,
    Address varchar(40) not null,
    City varchar(15) not null,
    State char(2) not null,
    Zip varchar(10) not null,
    Phone varchar(15) not null,
    Balance decimal(5,2) not null default 0.00,
    constraint pk_tblCustomer primary key(CustID asc)
);

/***** create Drivers Table *****/
/****** 2 ******/
drop  table if exists tblDriver;
create table tblDriver(
	DriverID int auto_increment not null,
    DriverFirst varchar(15) not null,
    DriverLast varchar(15) not null,
    SSN varchar(15) not null,
    DOB date not null,
    StartDate datetime not null,
    EndDate datetime  null,
    Address varchar(60) not null,
    City varchar(15) not null,
    State varchar(10) not null,
    Zip varchar(15) not null,
    Phone varchar(15) not null,
    Cell varchar(15) not null,
    MileageRate decimal(4,2)  null,
    Review datetime not null,
    DrivingRecord varchar(2)   null,
    constraint pk_tblDriver primary key(DriverID asc)
     
);

/***** create Employee Table *****/
/****** 3 ******/
drop table if exists tblEmployee;
create table tblEmployee(
	EmpID int auto_increment not null,
    EmpFirst varchar(15) not null,
    EmpLast varchar(15) not null,
    WarehouseID varchar(10) not null,
    SSN varchar(10) not null,
    DOB date not null,
    StartDate date not null,
    EndDate date null,
    Address varchar(40) not null,
    City varchar(15) not null,
    State char(2) not null,
    Zip varchar(15) not null,
    PositionID int not null,
    Memo varchar(20) null,
    Phone varchar(15) not null,
    Cell varchar(15) not null,
    Salary decimal(10,2) null,
    HourlyRat decimal(3,2) null,
    Review date not null,
    constraint pk_tblEmployee primary key(EmpID asc)
  );
  
  /***** create Warehouses Table *****/
/****** 4 ******/
drop table if exists tblWarehouse;
create table tblWarehouse(
	WarehouseID varchar(5) not null,
    Address varchar(40) not null,
    City varchar(15) not null,
    State char(2) not null,
    Zip varchar(15) not null,
    Phone varchar(15) not null,
    ClimateControl varchar(10) not null,
    SecurityGate varchar(10) not null,
    constraint pk_tblWarehouse primary key(WarehouseID asc)
    
);

/***** create Vehicle Table *****/
/****** 5 ******/
drop table if exists tblVehicle;
create table tblVehicle(
	VehicleID varchar(10) not null,
    LicensePlateNum varchar(10) not null,
    Axle int not null,
    Color varchar(10) not null,
    constraint pk_tblVehicle primary key(VehicleID asc)
);

/***** create Unit Rental Table *****/
/****** 6 ******/ 
drop table if exists tblUnitRental;
create table tblUnitRental (
	CustID int  not null,
    WarehouseID varchar(5) not null,
    UnitID varchar(5) not null,
    DateIn date not null,
    DateOut date null,
    constraint pk_tblUnitRental primary key(
    CustID asc,
    WarehouseID asc,
    UnitID asc
    )
    
);

/***** create Storage Unit Table *****/
/****** 7 ******/ 
drop table if exists tblStorageUnit;
create table tblStorageUnit(
	UnitID varchar(5) not null,
    WarehouseID varchar(5) not null,
    UnitSize varchar(10) not null,
    Rent decimal(5,2),
    constraint pk_tblStorageUnit primary key(
    UnitID asc,
    WarehouseID asc
    )
);

/***** create Job Position Table *****/
/****** 8 ******/ 
drop table if exists tblPosition;
create table tblPosition(
	PositionID int auto_increment not null,
    Title varchar(20) not null,
    constraint pk_tblPosition primary key(PositionID asc)
);

/***** create Job Order Table *****/
/****** 9 ******/
drop table if exists tblJobOrder;
create table tblJobOrder(
	JobID int auto_increment  not null,
    CustID int  not null,
    MoveDate date not null,
    FromAddress varchar(40) not null,
    FromCity varchar(10) not null,
    FromState varchar(5) not null,
    ToAddress varchar(40) not null,
    ToCity varchar(10) not null,
    ToState varchar(5) not null,
    DistanceEst int not null,
    WeightEst int not null,
    Packing varchar(6) not null,
    Heavy varchar(6) not null,
    Storage varchar(6) not null,
    constraint pk_tblJobOrder primary key(JobID asc)
);
alter table tblJobOrder auto_increment=2;
	

/***** create Job Detail Table *****/
/****** 10 ******/
drop table if exists tblJobDetail;
create table tblJobDetail(
	JobID int auto_increment  not null,
    VehicleID varchar(10) not null,
    DriverID int  not null,
    MileageActual smallint not null,
    WeightActual smallint not null,
    constraint pk_tblJobDetail primary key(JobID asc)
);
alter table tblJobDetail auto_increment=2;