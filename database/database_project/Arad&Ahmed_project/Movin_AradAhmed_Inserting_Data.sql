/*Movin_AradAhmed_inserting data
Script Data:august  2017
Developed by:Arad & Ahmed
*/
/*switch to the data base*/
use MovinOn_AradAhmed;

/***** 1 Loading Data to tblcustomer*****/

load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/Customer.csv'
into table tblcustomer
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblcustomer;

/***** 2 Loading Data to tblposition*****/

load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/JobPositionData.csv'
into table tblposition
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblposition;

/***** 3 Loading Data to tblwarehouse*****/

load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/WarehousesData.csv'
into table tblwarehouse
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblwarehouse;


/***** 4 Loading Data to tbldriver*****/

load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/DriversData.csv'
into table tbldriver
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines
;


select *
from tbldriver;

/***** 5 Loading Data to tblvehicle*****/

load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/VehiclesData.csv'
into table tblvehicle
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblvehicle;

/***** 6 Loading Data to tblUnitRentalsData*****/


load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/UnitRentalsData1.csv'
into table tblunitrental
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines
;


select *
from tblunitrental;

/***** 7 Loading Data to tblstorageunit*****/


load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/StorageUnitsData7.csv'
into table tblstorageunit
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblstorageunit;

/***** 8 Loading Data to tblemployee*****/

load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/EmployeesData.csv'
into table tblemployee
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

select *
from tblemployee;

/***** 9 Loading Data to tbljoborder*****/

load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/JobOrdersData.csv'
into table tbljoborder
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines;

 -- delete from tbljoborder
	-- where JobID between 2 and 29;

 
select *
from tbljoborder;


/***** 10 Loading Data to tbljobdetail*****/


 delete from tbljobdetail
	 where JobID between 2 and 29;
load data local infile 'C:/Users/arad/Documents/john abote/database/Arad&Ahmed_project/data/JobDetailsData.csv'
into table tbljobdetail
fields terminated by','
enclosed by '"'
lines terminated by '\r\n'
ignore 1 lines 
;

select *
from tbljobdetail;
