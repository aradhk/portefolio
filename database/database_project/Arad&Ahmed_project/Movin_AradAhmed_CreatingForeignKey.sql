/*Movin_AradAhmed_creating ForeignKey.sql
Script Data:august  2017
Developed by:Arad & Ahmed
*/
/*switch to the data base*/
use MovinOn_AradAhmed;

/* to add foreign key to the tables of MovingON database */
/*1. between tblunitrental and tblcustomer tables*/
-- remove if it exists

-- alter table tblunitrental if 
-- drop foreign key fk_tblunitrental_tblcustomer  
;
-- difine foreign key
alter table tblunitrental 
add constraint fk_tblcustomer_tblunitrental foreign key (CustID)
references tblcustomer (CustID)
;

/*2. between tblunitrental and tblwarehouse tables*/
-- remove if it exists

-- alter table tblunitrental 
-- drop foreign key fk_tblunitrental_tblwarehouse
;
-- difine foreign key
alter table tblunitrental 
add constraint fk_tblunitrental_tblwarehouse foreign key (WarehouseID)
references tblwarehouse(WarehouseID)
;
/*3. between tbljoborder and tblcustomer tables*/
-- remove if it exists

-- alter table tbljoborder
-- drop foreign key fk_tbljoborder_tblcustomer
;
-- difine foreign key
alter table tbljoborder 
add constraint fk_tbljoborder_tblcustomer foreign key (CustID)
references tblcustomer(CustID)
;
/*4. between tbljobdetail and tblvehical  tables*/
-- remove if it exists

-- alter table tbljobdetail
-- drop foreign key fk_tbljobdetail_tblvehical
;
-- difine foreign key
alter table tbljobdetail
add constraint fk_tbljobdetail_tblvehical foreign key (VehicleId)
references tblvehicle(VehicleId)
;
/*5. between tbljobdetail and tbldriver  tables*/
-- remove if it exists

-- alter table tbljobdetail
-- drop foreign key fk_tbljobdetail_tbldriver
;
-- difine foreign key
alter table tbljobdetail
add constraint fk_tbljobdetail_tbldriver foreign key (DriverID)
references tbldriver(DriverID)
;
/*6. between tblstorageunit and tblwarehouse  tables*/
-- remove if it exists

-- alter table tblstorageunit
-- drop foreign key fk_tblstorageunit_tblwarehouse
;
-- difine foreign key
alter table tblstorageunit
add constraint fk_tblstorageunit_tblwarehouse foreign key (WarehouseID)
references tblwarehouse(WarehouseID)
;
/*7. between tblemployee and tblwarehouse  tables*/
-- remove if it exists

-- alter table tblemployee
-- drop foreign key fk_tblemployee_tblwarehouse
;
-- difine foreign key
alter table tblemployee
add constraint fk_tblemployee_tblwarehouse foreign key (WarehouseID)
references tblwarehouse(WarehouseID)
;
/*8. between tblemployee and tblposition  tables*/
-- remove if it exists

-- alter table tblemployee
-- drop foreign key fk_tblemployee_tblposition
;
-- difine foreign key
alter table tblemployee
add constraint fk_tblemployee_tblposition foreign key (PositionID)
references tblposition(PositionID)
;
/*9. between tbljobdetail and  tbljoborder tables*/
-- remove if it exists

-- alter table tbljoborder
-- drop foreign key fk_tbljoborder_tbljobdetail
-- ;
-- difine foreign key for a one to one relation ship!
/*alter table tbljoborder
add constraint fk_tbljoborder_tbljobdetail foreign key (JobID)
references tbljobdetail(JobID)
;*/









