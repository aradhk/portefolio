﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace BankingApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UserClass user = new UserClass();
        DataBaseClass dataBase = new DataBaseClass();


        string connString = ConfigurationManager.ConnectionStrings["BankingApplication.Properties.Settings.BankDatabaseConnectionString"].ConnectionString;
        DataTable userInfo = new DataTable();
        DataTable accountInfo = new DataTable();
        DataSet dataSet = new DataSet();
        public string id;
        string accountNumber;
        public int Log = 1;

        public string currentAmonut;
        public string newAmonut;


        public MainWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Height;
            id = txbId.Text;




        }
        public MainWindow(string inputId)
        {
            InitializeComponent();

            this.SizeToContent = SizeToContent.Height;
            this.id = inputId;

            showClient();



        }

        private void showClient()
        {
            using (SqlConnection connection = new SqlConnection(connString))
            {
                String userComString = "Select * from Users where Id=" + txbId.Text;

                SqlCommand userSqlCom = new SqlCommand(userComString, connection);
                userSqlCom.CommandType = CommandType.Text;
                SqlDataAdapter dataAdapter = new SqlDataAdapter(userSqlCom);
                dataAdapter.Fill(dataSet, "userInfo");
                userInfo = dataSet.Tables["userInfo"];
                accountNumber = userInfo.Rows[0]["AccountNumber"].ToString();
                txbId.Text = userInfo.Rows[0]["ID"].ToString();
                txbName.Text = userInfo.Rows[0]["Name"].ToString();

                String accountComString = "Select * from Accounts where AccountNumber=" + accountNumber;
                SqlCommand accountSqlCom = new SqlCommand(accountComString, connection);
                accountSqlCom.CommandType = CommandType.Text;
                SqlDataAdapter dataAdapter2 = new SqlDataAdapter(accountSqlCom);
                dataAdapter2.Fill(dataSet, "accountInfo");
                accountInfo = dataSet.Tables["accountInfo"];
                txtBalance.Text = accountInfo.Rows[0]["Balance"].ToString();

            }
        }
        public void saveToFile(string account, string type, string oldamount, string curAmount)
        {
            var transactionTime = DateTime.Now;
            string time = transactionTime.ToString("[yy/MM/dd|hh:mm:ss]");

            string fileName = "transactions.txt";
            string saveOut = "[" + transactionTime + "]" + "#" + account + "|" + type + "|" + oldamount+"$" + "|" + curAmount+"$";
            StreamWriter outFile = File.AppendText(fileName);
            Console.WriteLine(saveOut);
            outFile.WriteLine($"{saveOut}");

            outFile.Close();
        }


        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (Log == 1)
            {

                LoginWindow loginWindow = new LoginWindow(id);
                loginWindow.ShowDialog();
                Log = 2;

                btnLogin.Content = "Log Out";



                txbId.Text = loginWindow.id;
                dataBase.SelectRecordById(txbId.Text);
                txbName.Text = dataBase.inputName;
                txtBalance.Text = dataBase.inputBalance;
                

                txtBalance.IsEnabled = true;
                txtAmount.IsEnabled = true;
                btnDeposit.IsEnabled = true;
                btnWithdraw.IsEnabled = true;




            }
            else if (Log == 2)
            {
                Log = 1;
                btnLogin.Content = "Login";
                id = "";
                txbId.Text = "";
                txbName.Text = "";
                txtBalance.Text = "";
                txtAmount.Text = "";
                txtBalance.IsEnabled = false;
                txtAmount.IsEnabled = false;
                btnDeposit.IsEnabled = false;
                btnWithdraw.IsEnabled = false;

            }



        }

        private void btnWithdraw_Click(object sender, RoutedEventArgs e)
        {
            currentAmonut = txtBalance.Text;
            
            showClient();
            if (String.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Please enter the amount");
            }
            else
            {

                try
                {
                    decimal amount = Decimal.Parse(txtAmount.Text);
                    if (decimal.Parse(accountInfo.Rows[0]["Balance"].ToString()) < amount)
                    {
                        MessageBox.Show("You can not withdraw more than $" + txtBalance.Text);
                    }
                    else
                    {
                        accountInfo.Rows[0]["Balance"] = decimal.Parse(accountInfo.Rows[0]["Balance"].ToString()) - amount;
                        string amountString = "Update Accounts SET Balance=" + accountInfo.Rows[0]["Balance"].ToString() + "WHERE AccountNumber=" + accountNumber;
                        ChangeAccounts(amountString);
                        MessageBox.Show("Transaction successful!");
                        showClient();

                        txtAmount.Text = "";
                        newAmonut = txtBalance.Text;
                        saveToFile(accountNumber, "W", currentAmonut, newAmonut);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }


        private void ChangeAccounts(String commandString)
        {
            using (SqlConnection connection = new SqlConnection(connString))
            {

                SqlCommand command = new SqlCommand(commandString, connection);

                try
                {
                    connection.Open();
                    command.Connection = connection;
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();

                }

            }

        }

        private void btnDeposit_Click(object sender, RoutedEventArgs e)
        {
            showClient();
            currentAmonut = txtBalance.Text;
            if (String.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Amount can be only a number");
            }
            else
            {
                try
                {
                    decimal amount = Decimal.Parse(txtAmount.Text);
                    accountInfo.Rows[0]["Balance"] = decimal.Parse(accountInfo.Rows[0]["Balance"].ToString()) + amount;
                    string amountNew = "Update Accounts SET Balance=" + accountInfo.Rows[0]["Balance"].ToString() + "WHERE AccountNumber=" + accountNumber;
                    ChangeAccounts(amountNew);

                    MessageBox.Show("Transaction successful!");
                    showClient();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                newAmonut = txtBalance.Text;
                saveToFile(accountNumber, "D", currentAmonut, newAmonut);
                txtAmount.Text = "";
            }
        }
    }
}
