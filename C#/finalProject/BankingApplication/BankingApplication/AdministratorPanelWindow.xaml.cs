﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace BankingApplication
{
 
	public partial class AdministratorPanelWindow : Window
	{

		string conString = ConfigurationManager.ConnectionStrings["BankingApplication.Properties.Settings.BankDatabaseConnectionString"].ConnectionString;
		string commandString = "";
		DataSet dataSet = new DataSet();
		DataBaseClass dataBase = new DataBaseClass();
		DataTable dataTable = new DataTable("Users");

		string name2;
		string pass2;



		public AdministratorPanelWindow(string name, string password)
		{
			name2 = name;
			pass2 = password;
			InitializeComponent();
			selectAll();
			theDataGrid.ItemsSource = dataTable.DefaultView;
            this.SizeToContent = SizeToContent.Height;

        }
		


		private void btnAdd_Click(object sender, RoutedEventArgs e)
		{
			AddUserWindow addUserWindow = new AddUserWindow();
			addUserWindow.ShowDialog();
			theDataGrid.Items.Refresh();

			selectAll();
		}



		private void btnRemove_Click(object sender, RoutedEventArgs e)
		{
			string index = dataTable.Rows[theDataGrid.SelectedIndex]["Id"].ToString();

			using (SqlConnection connection = new SqlConnection(conString))
			{
				
				commandString = "DELETE FROM Users WHERE ID=" + index;
				SqlCommand command = new SqlCommand(commandString, connection);
				try
				{
					connection.Open();
					command.Connection = connection;
					command.ExecuteNonQuery();
					connection.Close();

				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					connection.Close();
				}
			}
			string index2 = dataTable.Rows[theDataGrid.SelectedIndex]["AccountNumber"].ToString();
			using (SqlConnection connection = new SqlConnection(conString))
			{

				commandString = "DELETE FROM Accounts WHERE AccountNumber=" + index2;
				SqlCommand command = new SqlCommand(commandString, connection);
				try
				{
					connection.Open();
					command.Connection = connection;
					command.ExecuteNonQuery();
					connection.Close();

				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					connection.Close();
				}
			}


			selectAll();
		}




		public void selectAll()
		{
			DataSet dataSet = new DataSet();
			using (SqlConnection connection = new SqlConnection(conString))
			{

				commandString = "SELECT Users.ID, Users.Name , Users.AccountNumber, Accounts.Balance FROM Users, Accounts WHERE Users.AccountNumber=Accounts.AccountNumber";

				SqlCommand command = new SqlCommand(commandString, connection);
				SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

				dataAdapter.Fill(dataSet, "Users");
				dataTable = dataSet.Tables["Users"];

				theDataGrid.ItemsSource = dataTable.DefaultView;
				theDataGrid.Items.Refresh();

			}
		}

		public AccountClass LoadLast()
		{
			using (SqlConnection connection = new SqlConnection(conString))
			{
				AccountClass account = new AccountClass();
				
				string sql = "SELECT TOP 1 * FROM[dbo].[Accounts] ORDER BY AccountNumber DESC";
				connection.Open();
				SqlCommand cmd = new SqlCommand(sql, connection);
				SqlDataReader reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					AccountClass result = new AccountClass();
					result.accountnumber = reader.GetInt32(0);

					connection.Close();

					return result;

					
				}

				connection.Close();
				return null;
			}
		}


		public void add()
		{

			using (SqlConnection connection = new SqlConnection(conString))
			{
				string index6 = "0";



				commandString = "INSERT INTO Accounts" + " (Balance)" + " VALUES('" + index6 + "')";
				SqlCommand command = new SqlCommand(commandString, connection);
				try
				{
					connection.Open();
					command.Connection = connection;
					command.ExecuteNonQuery();
					connection.Close();



				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					connection.Close();
				}

			}

			AccountClass account = new AccountClass();
			account = LoadLast();
			int acc = account.accountnumber;
			int acc2 = acc++;

			using (SqlConnection connection = new SqlConnection(conString))
			{

				string index3 = name2;
				string index4 = pass2;
				string index5 = acc2.ToString();

				commandString = "INSERT INTO Users" + " (Name , Password, AccountNumber)" + " VALUES('" + index3 + "','" + index4 + "','" + index5 + "')";
				SqlCommand command = new SqlCommand(commandString, connection);        
				try
				{
					connection.Open();
					command.Connection = connection;
					command.ExecuteNonQuery();
					connection.Close();



				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					connection.Close();
				}

			}
			//MessageBox.Show("User successfully added!");
			
			selectAll();

		}




		public UserClass LoadSelectedUser()

		{
           

			string id = dataTable.Rows[theDataGrid.SelectedIndex]["Id"].ToString();

			using (SqlConnection connection = new SqlConnection(conString))
			{
				
				string sql = "SELECT Users.ID, Users.Name , users.AccountNumber FROM Users WHERE ID=" + id;
				connection.Open();
				SqlCommand cmd = new SqlCommand(sql, connection);
				SqlDataReader reader = cmd.ExecuteReader();
				UserClass result = new UserClass();
				while (reader.Read())
				{

					result.id = reader.GetInt32(0);
					result.name = reader.GetString(1);
					string[] ssize = result.name.Split(null);
					result.name = ssize[0];
					result.accounNumber = reader.GetInt32(2);


				}

				connection.Close();
				return result;
			}
		}


		public void UpdateName(int id, string name)
		{
			using (SqlConnection connection = new SqlConnection(conString))
			{
				
				String sql = "UPDATE Users SET Name ='" + name + "' WHERE ID =" + id;
				connection.Open();
				SqlCommand cmd = new SqlCommand(sql, connection);
				SqlDataReader reader = cmd.ExecuteReader();
				connection.Close();
			}
		}


		public void UpdatePassword(int id, string password)
		{
			using (SqlConnection connection = new SqlConnection(conString))
			{

				String sql = "UPDATE Users SET Password ='" + password + "' WHERE ID =" + id;
				connection.Open();
				SqlCommand cmd = new SqlCommand(sql, connection);
				SqlDataReader reader = cmd.ExecuteReader();
				connection.Close();
			}
		}




		private void btnEdit_Click(object sender, RoutedEventArgs e)
		{
            try
            {
                UserClass userClass = new UserClass();
                userClass = LoadSelectedUser();
                int id = userClass.id;

                int accountNumber = userClass.accounNumber;
                UpdateWindow updateWindow = new UpdateWindow(id, accountNumber);
                updateWindow.ShowDialog();
                theDataGrid.Items.Refresh();

                selectAll();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                return;

            }

        }
	}
}
