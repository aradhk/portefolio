﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BankingApplication
{
	
	public partial class UpdateWindow : Window
	{
		public UpdateWindow(int id,int accountNumber)
		{
			InitializeComponent();
			this.SizeToContent = SizeToContent.Height;
			
			string id2 = id.ToString();
			string accountNumber2 = accountNumber.ToString();
			txtBlockIDUpdate.Text = id2;
			txtBlockAccountNumberUpdate.Text = accountNumber2;
		}

		public void btnUpdateUserUpdate_Click(object sender, RoutedEventArgs e)
		{

			string name = txtBoxNewNameUpdate.Text;
			string password = txtBoxNewPasswordUpdate.Password.ToString();
			int id =Int32.Parse(txtBlockIDUpdate.Text);
			AdministratorPanelWindow administratorPanelWindow = new AdministratorPanelWindow(name, password);
			administratorPanelWindow.UpdateName(id, name);
			administratorPanelWindow.UpdatePassword(id, password);
			this.Close();
		}
	}
}
