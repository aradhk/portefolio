﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace BankingApplication
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {

        DataBaseClass dataBase = new DataBaseClass();
        public string id { get; set; }
        string name;
        string password;
        string result = "";


        UserClass user = new UserClass();

        public LoginWindow(string inputId)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Height;
            this.id = inputId;
        }



        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            id = txtId.Text;
            password = pasPassword.Password;
            result = dataBase.login(id, password);




            if (result == "admin")
            {
                user.id = Int32.Parse(id);
                this.Close();
                AdministratorPanelWindow administratorPanelWindow = new AdministratorPanelWindow(name, password);
                administratorPanelWindow.ShowDialog();

                this.Close();

            }
            else if (result == "client")
            {

                user.id = Int32.Parse(id);
                this.Close();

            }
            else
            {
                txtId.Text = "";
                pasPassword.Password = "";
            }


        }
    }
}
