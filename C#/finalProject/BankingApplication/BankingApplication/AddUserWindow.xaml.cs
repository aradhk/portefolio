﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BankingApplication
{
   
	public partial class AddUserWindow : Window
	{
	//	DataBaseClass dataBase = new DataBaseClass();
	 //   public string name;
	 //   public string password;
		public AddUserWindow()
		{
			InitializeComponent();
			this.SizeToContent = SizeToContent.Height;
		}

		private void btnLogin_Click(object sender, RoutedEventArgs e)
		{
			string name = txtName.Text;
			string password = pasPassword.Password.ToString();
			
			AdministratorPanelWindow administratorPanelWindow = new AdministratorPanelWindow(name, password);
			administratorPanelWindow.add();
			this.Close();

			//  dataBase.AddUser(name, password);

		}
	}
}
