﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BankingApplication
{
    class DataBaseClass
    {
        //define the variables
        string ConString = ConfigurationManager.ConnectionStrings["BankingApplication.Properties.Settings.BankDatabaseConnectionString"].ConnectionString;
        string CmdString = string.Empty;

		string name;
		string password;

        DataTable dtUsers = new DataTable("Users");
        DataTable dtAccounts = new DataTable("Accounts");
        DataTable dtSelectInfo = new DataTable("SelectInfo");
        DataTable dtAccountInfo = new DataTable("AccountInfo");
        DataTable dtLogin = new DataTable("Login");
        

        public string inputId { get; set; }
        public string inputPassword { get; set; }
        public string inputName { get; set; }
        public string inputAccountNumber { get; set; }
        public string inputBalance { get; set; }

        public string adminId { get; set; }
        public string adminPassword { get; set; }
        public string rowExist { get; set; }

        UserClass user = new UserClass();
        AccountClass account = new AccountClass();
        public List<AllInfoClass> allInfo = new List<AllInfoClass>();
        //return username and password if they are valid

        public string login(string inputId, string inputPassword)
        {
            
            if (inputId == "" || inputPassword == "")
            {
                MessageBox.Show("Please Enter your ID and Password");
            }
            else
            {

                if (CheckLogin(inputId, inputPassword))
                {
                    if (inputId == "2")
                    {
                        rowExist = "admin";
                        inputId = "";
                        inputPassword = "";


                        //AdministratorPanelWindow administratorPanelWindow = new AdministratorPanelWindow();
                        //administratorPanelWindow.ShowDialog();



                    }
                    else
                    {
                        rowExist = "client";
                        inputId = "";
                        inputPassword = "";
                        //MainWindow mainWindow = new MainWindow(inputId);
                        //mainWindow.ShowDialog();




                    


                }

                }
                else
                {
                    MessageBox.Show("Invalid id or password");
                }
                inputId = "";
                inputPassword = "";
            }
            return rowExist;
        }


        //checking if username and password is valid
        private bool CheckLogin(String inputId, String inputPassword)
        {
            DataSet dataSet = new DataSet();
            try
            {

                using (SqlConnection connection = new SqlConnection(ConString))
                {
                    CmdString = "Select * from Users where Id=" + inputId + " AND Password='" + inputPassword + "'";

                    SqlCommand cmd = new SqlCommand(CmdString, connection);
                    cmd.CommandType = CommandType.Text;
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

                    dataAdapter.Fill(dataSet, "Login");
                    Console.WriteLine(CmdString);

                    if (dataSet.Tables["Login"].Rows.Count != 0)
                    {
                        String userName = dataSet.Tables["Login"].Rows[0]["Name"].ToString();

                        MessageBox.Show("Welcome! " + userName);
                        return true;

                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        public  DataTable  AdminSelectAllInfo()
        {
            string ConString = ConfigurationManager.ConnectionStrings["BankingApplication.Properties.Settings.BankDatabaseConnectionString"].ConnectionString;
            string CmdString = string.Empty;
            DataSet dataSet = new DataSet();
            DataTable dtAdmin = new DataTable("Admin");

            using (SqlConnection connection = new SqlConnection(ConString))
            {
                CmdString = "SELECT [U].[Id],[U].[Name],[U].[Password],[A].[Balance] FROM [USERS] AS [U] JOIN [ACCOUNTS] AS [A] ON [U].[ACCOUNTNUMBER]=[A].[ACCOUNTNUMBER]";
                SqlCommand command = new SqlCommand(CmdString, connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                dataAdapter.Fill(dataSet, "admin");
                dtAdmin = dataSet.Tables["admin"];
                
                
            }
            return dtAdmin;
        }

        public void AddUser(string inputName,string inputPassword)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConString))
                {
                    string name = inputName;
                    string password = inputPassword;
                    //int accountNumber;

                    string cmdInsertAccount = "INSERT INTO Accounts(Balance) VALUES(0)";

                    SqlCommand command1 = new SqlCommand(cmdInsertAccount, connection);
                    connection.Open();
                    command1.ExecuteNonQuery();
                    Console.WriteLine("after execute com1");
                    string cmdInsertUser = "INSERT INTO Users (Name, Password, AccountNumber) VALUES('" + name + "','" + password + "',(SELECT TOP 1 AccountNumber from Accounts ORDER BY AccountNumber DESC))";
                    SqlCommand command2 = new SqlCommand(cmdInsertUser, connection);
                    command2.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public void SelectRecordById(string inputId)

        {
            string ConString = ConfigurationManager.ConnectionStrings["BankingApplication.Properties.Settings.BankDatabaseConnectionString"].ConnectionString;
            string CmdString = string.Empty;
            DataSet dataSet = new DataSet();
            
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                String cmdUserString = "Select * from Users where Id=" + inputId;

                SqlCommand userSqlCom = new SqlCommand(cmdUserString, connection);
                userSqlCom.CommandType = CommandType.Text;
                SqlDataAdapter dataAdapter = new SqlDataAdapter(userSqlCom);
                dataAdapter.Fill(dataSet, "SelectInfo");
                dtSelectInfo = dataSet.Tables["SelectInfo"];
                inputAccountNumber = dtSelectInfo.Rows[0]["AccountNumber"].ToString();
                inputId = dtSelectInfo.Rows[0]["ID"].ToString();
                inputName = dtSelectInfo.Rows[0]["Name"].ToString();

                String cmdAccountString = "Select * from Accounts where AccountNumber=" + inputAccountNumber;
                SqlCommand accountSqlCom = new SqlCommand(cmdAccountString, connection);
                accountSqlCom.CommandType = CommandType.Text;
                SqlDataAdapter dataAdapter2 = new SqlDataAdapter(accountSqlCom);
                dataAdapter2.Fill(dataSet, "AccountInfo");
                dtAccountInfo = dataSet.Tables["AccountInfo"];
                inputBalance = dtAccountInfo.Rows[0]["Balance"].ToString();

            }
        }




        //     public void DeleteUser(int rowIndex)
        //     {
        //         string index = dataTable.Rows[theDataGrid.SelectedIndex]["id"].ToString();

        //using (SqlConnection connection = new SqlConnection(connectionString))
        //{
        ////	commandString = "DELETE Users FROM Users, Accounts where Users.AccountNumber=Accounts.AccountNumber AND ID=" + index;
        //	commandString = "DELETE FROM Users WHERE ID=" + index;
        //	SqlCommand command = new SqlCommand(commandString, connection);
        //	try
        //	{
        //		connection.Open();
        //		command.Connection = connection;
        //		command.ExecuteNonQuery();
        //		connection.Close();

        //	}
        //	catch (Exception ex)
        //	{
        //		MessageBox.Show(ex.Message);
        //		connection.Close();
        //	}
        //}

        //string index2 = dataTable.Rows[theDataGrid.SelectedIndex]["AccountNumber"].ToString();
        //using (SqlConnection connection = new SqlConnection(connectionString))
        //{

        //	commandString = "DELETE FROM Accounts WHERE AccountNumber=" + index2;
        //	SqlCommand command = new SqlCommand(commandString, connection);
        //	try
        //	{
        //		connection.Open();
        //		command.Connection = connection;
        //		command.ExecuteNonQuery();
        //		connection.Close();

        //	}
        //	catch (Exception ex)
        //	{
        //		MessageBox.Show(ex.Message);
        //		connection.Close();
        //	}
        //}
        // }
    }
}
