﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApplication
{
    class AllInfoClass
    {
        public int id { get; set; }
        public string name { get; set; }
        public string accountNumber { get; set; }
        public decimal balance { get; set; }
    }
}
