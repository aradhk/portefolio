﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WpfApp10
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        string connectionString = ConfigurationManager.ConnectionStrings["WpfApp10.Properties.Settings.Database1ConnectionString"].ConnectionString;
        string commandString = "";
        DataTable dataTable = new DataTable("users");

        public MainWindow()
        {
            InitializeComponent();
            selectAll();
        }
        private void selectAll() { 
            DataSet dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                commandString = "SELECT ID , PASSWORD FROM ";
                SqlCommand command = new SqlCommand(commandString, connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                dataAdapter.Fill(dataSet, "users");
                dataTable = dataSet.Tables["users"];
                theDataGrid.ItemsSource = dataTable.DefaultView;
            }
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                commandString = "INSERT INTO USERS (NAME,CLASS) VALUES ('Lyn','DataBase')";
                SqlCommand command = new SqlCommand(commandString, connection);
                try
                {
                    connection.Open();
                    command.Connection = connection;
                    command.ExecuteNonQuery();
                    connection.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }
            selectAll();


        }

      

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            string index = dataTable.Rows[theDataGrid.SelectedIndex]["id"].ToString();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                commandString = "DELETE FROM USERS WHERE id="+index;
                SqlCommand command = new SqlCommand(commandString, connection);
                try
                {
                    connection.Open();
                    command.Connection = connection;
                    command.ExecuteNonQuery();
                    connection.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }
            selectAll();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                commandString = "UPDATE Users SET Name= 'rjf Finstone' WHERE id=1";
                SqlCommand command = new SqlCommand(commandString, connection);
                try
                {
                    connection.Open();
                    command.Connection = connection;
                    command.ExecuteNonQuery();
                    connection.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }
            selectAll();
        }
    }
}
