﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Xaml;

namespace DatabaseExample
{
    public partial class MainWindow : Window
    {
        string ConString = ConfigurationManager.ConnectionStrings["DatabaseExample.Properties.Settings.TheDatabaseConnectionString"].ConnectionString;
        string CmdString = string.Empty;

        DataTable dt = new DataTable("Users");
        public MainWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Height;

            selectAll();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddUserWindow win = new AddUserWindow();
            win.ShowDialog();
            User newUser = win.newUser;

            using (SqlConnection connection = new SqlConnection(ConString))
            {
                CmdString = "INSERT INTO Users (Name, Class) VALUES (\'" + newUser.Name + "\',\'" + newUser.Class + "\')";
                SqlCommand cmd = new SqlCommand(CmdString, connection);

                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
                    //MessageBox.Show("DataBase updated!");
                    connection.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }

            selectAll();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                CmdString = "UPDATE Users SET Name=\'Fred\' WHERE id=1";
                SqlCommand cmd = new SqlCommand(CmdString, connection);

                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
                    //MessageBox.Show("DataBase updated!");
                    connection.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }

            selectAll();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            string index = dt.Rows[theDataGrid.SelectedIndex]["id"].ToString();

            using (SqlConnection connection = new SqlConnection(ConString))
            {
                CmdString = "Delete FROM Users WHERE id="+index;
                SqlCommand cmd = new SqlCommand(CmdString, connection);

                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
                    //MessageBox.Show("DataBase updated!");
                    connection.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }

            selectAll();
        }

        private void selectAll()
        {
            
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConString))
            {
                //CmdString = "SELECT * FROM Users WHERE Name='Bruce Banner'";
                //CmdString = "UPDATE Users SET Name='Vik' WHERE id=1";
                CmdString = "SELECT * FROM Users";
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(ds, "Users");
                dt = ds.Tables["Users"];

                //txtName.Text = dt.Rows[0]["Name"].ToString();
                
                theDataGrid.ItemsSource = dt.DefaultView;
                theDataGrid.Items.Refresh();
            }
        }
    }
}
