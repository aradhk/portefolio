﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MultiForm_File_IO_Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Person> people = new List<Person>();
        String fileName = "";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.ShowDialog();
            
            try
            {
                fileName = openFileDialog.FileName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (File.Exists(fileName) && fileName.EndsWith(".csv"))
            {
                loadCSV(fileName);

                btnShow.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Invalid Filename.");
            }
        }

        public void loadCSV(String theFileName)
        {
            StreamReader inFile = new StreamReader(theFileName);

            while (!inFile.EndOfStream)
            {
                String aLine = inFile.ReadLine();
                String[] theParts = aLine.Split(',');
                Person aPerson = new Person();
                aPerson.LastName = theParts[0];
                aPerson.FirstName = theParts[1];
                aPerson.Age = Int32.Parse(theParts[2]);
                people.Add(aPerson);
            }

            inFile.Close();
            MessageBox.Show("File has been loaded.");
            dataGridNames.ItemsSource = people;
        }

        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            DataWindow dataWindow = new DataWindow(people);
            dataWindow.ShowDialog();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.ShowDialog();

            try
            {
                fileName = saveFileDialog.FileName;

                StreamWriter outFile = new StreamWriter(fileName);

                foreach (Person person in people)
                {
                    outFile.WriteLine($"{person.FirstName},{person.LastName},{person.Age},");
                }

                outFile.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (Person person in people)
            {
                //MessageBox.Show(person.Age.ToString() + "\n");
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //people.Add(new Person { FirstName = "Bruce", LastName = "Banner", Age = 40 });
            //people.Add(new Person { FirstName = "Tony", LastName = "Start", Age = 35 });
            //people.Add(new Person { FirstName = "Peter", LastName = "Parker", Age = 21 });

            //dataGridNames.ItemsSource = people;
        }
    }
}
