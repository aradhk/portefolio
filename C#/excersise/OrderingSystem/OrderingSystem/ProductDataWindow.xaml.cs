﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OrderingSystem
{
    public partial class ProductDataWindow : Window
    {
        FileManager fileManager = new FileManager();
        public List<Product> products { get; set; }

        public ProductDataWindow(List<Product> existingProducts)
        {
            InitializeComponent();
            
            this.products = existingProducts;
            dataGridProducts.ItemsSource = products;
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            products = fileManager.loadProductFile();
            dataGridProducts.ItemsSource = products;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            fileManager.saveProductFile(products);
        }

        private void btnSaveAs_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
