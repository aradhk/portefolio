﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrderingSystem
{
    public partial class MainWindow : Window
    {
        List<Product> products;
        List<Customer> customers;

        public MainWindow()
        {
            InitializeComponent();

            this.SizeToContent = SizeToContent.Height;
        }

        private void btnOpenCustomersWindow_Click(object sender, RoutedEventArgs e)
        {
            CustomerDataWindow customerDataWindow = new CustomerDataWindow(customers);
            customerDataWindow.ShowDialog();
            customers = customerDataWindow.customers;
        }

        private void btnOpenProductsWindow_Click(object sender, RoutedEventArgs e)
        {
            ProductDataWindow productDataWindow = new ProductDataWindow(products);
            productDataWindow.ShowDialog();
            products = productDataWindow.products;
        }
    }
}
