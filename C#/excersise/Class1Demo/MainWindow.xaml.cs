﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Class1Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Person> people = new List<Person>();

        public MainWindow()
        {
            InitializeComponent();

            people.Add(new Person { FirstName = "Natasha", LastName = "Romanoff" });
            people.Add(new Person { FirstName = "Bruce", LastName = "Banner" });
            people.Add(new Person { FirstName = "Tony", LastName = "Stark" });

            cbxNames.ItemsSource = people;
        }

        private void btnRun_Click(object sender, RoutedEventArgs e)
        {
            String firstName = txtFirstName.Text;
            MessageBox.Show($"Hello { firstName }");
        }

        private void btnChangeHeader_Click(object sender, RoutedEventArgs e)
        {
            txtblkHeader.Text = "Vik";
        }

        private void btnClearList_Click(object sender, RoutedEventArgs e)
        {
            chkApple.IsChecked = false;
            chkBanana.IsChecked = false;
            chkOrange.IsChecked = false;
        }
    }
}
