﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TravelAgency.Models
{
    public class TravelAgencyContext : DbContext
    {
        public TravelAgencyContext (DbContextOptions<TravelAgencyContext> options)
            : base(options)
        {
        }

        public DbSet<TravelAgency.Models.Vacation> Vacation { get; set; }
    }
}
