﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginPage
{
    public partial class SiteMaster : MasterPage
    {
        public Boolean log = false;
        protected void Page_Load(object sender, EventArgs e)

        {
            if (log == false)
            {
                if (Session["IsOpen"] != null)
                {
                    aLog.InnerText = "logOff";
                    log = true;
                }
            }
            else
            {
                Session["IsOpen"] = null;
                Response.Redirect("Default");
            }

            
            

        }
        protected void aLog_Click(object sender, EventArgs e)
        {
            if (log == true)
            {
                aLog.InnerText = "login";
                Session["IsOpen"] = null;
                //Response.Redirect("Default");

            }
        }
    }
}