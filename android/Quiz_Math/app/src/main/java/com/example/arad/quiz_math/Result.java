package com.example.arad.quiz_math;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Result extends AppCompatActivity implements View.OnClickListener{





    RadioGroup rbg;
    TextView textView1, textViewState;
    RadioButton RadioButtonRight, RadioButtonWrong, RadioButtonAll;
    Button back;
    String resultAll, resultR, resultW, statistic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        initialize();
    }

    private void initialize() {


        rbg = findViewById(R.id.rbg);
        rbg.setOnClickListener(this);


        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        //Intent intent = getIntent();


        RadioButtonRight = findViewById(R.id.rbtrRight);
        RadioButtonRight.setOnClickListener(this);

        RadioButtonWrong = findViewById(R.id.rbtWrong);
        RadioButtonWrong.setOnClickListener(this);

        RadioButtonAll = findViewById(R.id.rbtAll);
        RadioButtonAll.setOnClickListener(this);

        textView1 = findViewById(R.id.textView1);

        textViewState = findViewById(R.id.textViewState);



        if (getIntent().getExtras() != null) {
//             resultR = getIntent().getStringExtra("resultR");
//            resultW = getIntent().getStringExtra("resultW");

            String  results = getIntent().getStringExtra("results");
            String score = getIntent().getStringExtra("scores");
            textView1.setText(results);
            textViewState.setText(score);


        }
    }




    @Override
    public void onClick(View view) {

        switch(view.getId())
        {

            case R.id.back:

                goToMain();

                break;
//            case(R.id.rbtrRight):
//                showRigth();
//                break;
//            case(R.id.rbtWrong):
//                showWrong();
//                break;
//            case(R.id.rbtAll):
//                showall();
//                break;

        }

    }
//    public void showRigth(){
//        textView1.setText(getIntent().getStringExtra("resultR"));
//
//    }
//    public void showWrong(){
//        textView1.setText(getIntent().getStringExtra("resultW"));
//
//    }
//    public void showall(){
//        textView1.setText(getIntent().getStringExtra("resultR")+getIntent().getStringExtra("resultW");
//
//    }

    private void goToMain() {


        finish();

    }









//    @Override
//    public void onClick(View view) {
//
//            int selectRBG = rbg.getCheckedRadioButtonId();
//
//            switch (selectRBG) {
//                case R.id.rightAnswers : textView2.setText(resultR);break;
//                case R.id.wrongAnswers: textView2.setText(resultW);break;
//                case R.id.allAnswers :  textView2.setText(resultAll);break;
//            }
//
//        }
    }
