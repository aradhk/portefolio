package com.example.arad.quiz_math;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,TextWatcher{


    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, dot, minus, generate, validate, clear, score, finish;
    TextView textView,userAnswer;
    EditText editText;

    int numRight=0;
    int numWrong=0;
    float result;

//    int allTasks=0, rightTask=0;
//    float stat=0.00f;
    ArrayList<OperationQuiz> allQuiz;
    String quiz;

//    int randOp;
//    String message, saveResult, rightResult, wrongResult, statistic;
//
//
//    String regex = "^\\d*\\.\\d+|\\d+\\.\\d*$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }
    private void initialize() {

        b1 = findViewById(R.id.b1);
        b1.setOnClickListener(this);

        b2 = findViewById(R.id.b2);
        b2.setOnClickListener(this);

        b3 = findViewById(R.id.b3);
        b3.setOnClickListener(this);

        b4 = findViewById(R.id.b4);
        b4.setOnClickListener(this);

        b5 = findViewById(R.id.b5);
        b5.setOnClickListener(this);

        b6 = findViewById(R.id.b6);
        b6.setOnClickListener(this);

        b7 = findViewById(R.id.b7);
        b7.setOnClickListener(this);

        b8 = findViewById(R.id.b8);
        b8.setOnClickListener(this);

        b9 = findViewById(R.id.b9);
        b9.setOnClickListener(this);

        b0 = findViewById(R.id.b0);
        b0.setOnClickListener(this);

        dot = findViewById(R.id.dot);
        dot.setOnClickListener(this);

        minus = findViewById(R.id.minus);
        minus.setOnClickListener(this);

        generate = findViewById(R.id.generate);
        generate.setOnClickListener(this);

        validate = findViewById(R.id.validate);
        validate.setOnClickListener(this);

        clear = findViewById(R.id.clear);
        clear.setOnClickListener(this);

        score = findViewById(R.id.score);
        score.setOnClickListener(this);

        finish = findViewById(R.id.finish);
        finish.setOnClickListener(this);

        textView = findViewById(R.id.textView);
        //textView.setOnClickListener(this);

        userAnswer = findViewById(R.id.userAnswer);
        userAnswer.setOnClickListener(this);

        editText = findViewById(R.id.editText);
        editText.setOnClickListener(this);

      allQuiz=new ArrayList<>();

//        allTasks=0;
//        rightTask=0;
//        stat=0.00f;
//
//
//
//        rightResult = "";
//        saveResult = "";
//        wrongResult = "";

    }



    @Override
    public void onClick(View view) {

        int btnID = view.getId();
        switch (btnID){

            case R.id.generate:
                goGenerate();
                break;

            case R.id.validate:
                goValidate();
                break;

            case R.id.score:
                goScore();
                break;

            case R.id.clear:
                clearAnswer();
                break;

            case R.id.finish:
                finish();
                break;

            case R.id.b0:
                addNumber("0");
                break;

            case R.id.b1:
                addNumber("1");
                break;

            case R.id.b2:
                addNumber("2");
                break;
            case R.id.b3:
                addNumber("3");
                break;
            case R.id.b4:
                addNumber("4");
                break;
            case R.id.b5:
                addNumber("5");
                break;
            case R.id.b6:
                addNumber("6");
                break;
            case R.id.b7:
                addNumber("7");
                break;
            case R.id.b8:
                addNumber("8");
                break;
            case R.id.b9:
                addNumber("9");
                break;
            case R.id.dot:
                addNumber(".");
                break;
            case R.id.minus:
                addNumber("-");
                break;


        }
    }

//    private void gominus(char c) {
//
//
//
//            editText.append(minus.getText().toString());
//
//
//
//    }

//    private void goScore() {
    ////////////////
//    }

    public void goGenerate() {



        textView.setText("");
        editText.setText("");
        Random r = new Random();
        int n1 = r.nextInt(10);
        int n2 = r.nextInt(10) ;
        int n3 = r.nextInt(4 );
        String op = "";
        switch (n3){
            case 0:
            op = "+";
            result = n1 + n2;
            break;

            case 1:
            op = "*";
            result = n1 * n2;
            break;

            case 2:
            op = "-";
            result = n1 - n2;
            break;

            case 3:
            op = "/";
            result =(float) n1 / n2;


            break;
        }

        quiz=Integer.toString(n1) +  op  + Integer.toString(n2);
        String question = Integer.toString(n1) +  op  + Integer.toString(n2)+" = ?";
        textView.setText(question);
        validate.setEnabled(true);


    }
///////////////////////////////////////////////////////


    private void goValidate(){

        String quizResult;
        float answer=Float.valueOf(editText.getText().toString());
        if(Math.abs(answer-result)< 0.00001){
            quizResult="Correct!";

            Toast.makeText(this,quizResult, Toast.LENGTH_LONG).show();

            numRight++;

            //System.out.println(numRight);
            OperationQuiz matquiz = new OperationQuiz(quiz,result,editText.getText().toString(),"right");
            allQuiz.add(matquiz);

            userAnswer.setText("your answer is correct!");
           // userAnswer.setText(Integer.toString(numRight));

        }else {
            quizResult = "Incorrect!";
            Toast.makeText(this,quizResult, Toast.LENGTH_LONG).show();

            numWrong++;

            OperationQuiz matquiz = new OperationQuiz(quiz,result,editText.getText().toString(),"wrong");
            allQuiz.add(matquiz);

            userAnswer.setText("your answer is incorrect!");
            //userAnswer.setText(Integer.toString(numWrong));
            score.setEnabled(true);
        }



    }








    public void goScore() {



        if(allQuiz.size() > 0) {


            StringBuilder myStringBuilder = new StringBuilder("");
//            StringBuilder myStringRight = new StringBuilder("");
//            StringBuilder myStringWrong = new StringBuilder("");



            for (OperationQuiz myQuiz : allQuiz) {

                myStringBuilder.append(myQuiz + "\n");

//                if(myQuiz.getmessage()=="right") {
//                    myStringRight.append(myQuiz + "\n");
//                }
//                else{
//                    myStringWrong.append(myQuiz+"\n");
//                }
            }

            int rightAnswerPercent= (100*numRight/(numRight+ numWrong));
            int wrongAnswerPercent = (100*numWrong/(numRight+ numWrong));

            String myScoreStr = String.format("Correct Answers: %d%%\nWrong answers: %d%%",rightAnswerPercent, wrongAnswerPercent);

            Intent intent = new Intent(this, Result.class);
//            intent.putExtra("resultsR", myStringRight.toString());
//            intent.putExtra("resultsW", myStringWrong.toString());

            intent.putExtra("results",myStringBuilder.toString());
            intent.putExtra("scores", myScoreStr);
            startActivity(intent);
            clearAnswer();
        }
        else
        {

            Toast.makeText(this, "No Score exist.", Toast.LENGTH_SHORT).show();
            userAnswer.setText("please start to play!");

        }


    }

    private void clearAnswer() {

        editText.setHint("0");
        editText.setText("");
        textView.setText("0+0=?");
        userAnswer.setText("Start!");
        validate.setEnabled(false);
        score.setEnabled(false);

    }


    private void addNumber(String s) {

        String mystring = editText.getText().toString();
        //String s=String.valueOf(n);
        if(s=="." && (mystring.contains("."))) {
            return;
        }else if(s=="-") {
                if (mystring.contains("-")) {
                    return;
                }
                editText.setText(s + editText.getText());
        }else if(mystring=="0" && s=="0") {
            return;

        }else {


            editText.setText(editText.getText() + s);
        }
    }









    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        //validate.setEnabled(true);

    }
}
