package com.example.arad.quiz_math;

import android.support.annotation.NonNull;

/**
 * Created by arad on 2018-03-04.
 */

public class OperationQuiz {

    private String quiz;
    private float correct;
    private String answer;
    private String message;


    public String getquiz() {
        return quiz;
    }

    public void setquiz(String quiz) {
        this.quiz = quiz;
    }


    public float getcorrect() {
        return correct;
    }

    public void setcorrect(int result) {
        this.correct = correct;
    }


    public String getanswer() {
        return answer;
    }

    public void setanswer(String answer) {
        this.answer = answer;
    }


    public String getmessage() {
        return message;
    }

    public void setmessage(String answer) {
        this.message = message;
    }


    public OperationQuiz(String quiz, Float correct, String answer, String message) {
        this.quiz = quiz;
        this.correct = correct;
        this.answer = answer;
        this.message = message;
    }

    @Override
    public String toString() {
        String returnString;
        if (message == "right") {
            returnString = String.format("%s = %s \nYour answer is %s \n--------------------------- ", quiz, answer, message);


        } else {
            returnString = String.format("%s = %s \nYour answer is %s\nThe Correct Answer is %s \n---------------------------", quiz, answer, message, String.valueOf(correct));
        }

        return returnString;

    }
}





