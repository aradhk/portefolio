package day1.beans;

import javax.ejb.Stateless;

@Stateless
public class CalculatorBean implements CalculatorBeanRemote {
    
    
    
    @Override
    public double celToFah(double cel) {
	// return Celsius degrees converted to Fahrenheit
        return cel*9/5 + 32;
    }
}
