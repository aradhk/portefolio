package abbott.beans;

import javax.ejb.Remote;

@Remote
public interface ShoutBeanRemote {
    public void addShout(String shout);
    public String[] getAllShouts();
    
//    public void addTodo(Todo todo);
    
}
