package abbott.ejb;

import javax.ejb.Stateless;

@Stateless
public class HelloBean implements HelloBeanRemote {

    @Override
    public String sayHello() {
        System.out.println("FYI: sayHello was called");
        return "Hello from Strange Beans, the Giant is upon us.";
    }

}
