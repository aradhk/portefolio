package abbott.beans;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import static javax.ejb.LockType.READ;
import static javax.ejb.LockType.WRITE;
import javax.ejb.Singleton;

@Singleton
public class ShoutBean implements ShoutBeanRemote {

    private ArrayList<String> shoutList;
    
    @PostConstruct
    void init() {
        shoutList = new ArrayList<>();
    }
    
    @Lock(WRITE)
    @Override
    public void addShout(String shout) {
        System.out.println("NNN: addShout called with: " + shout);
        shoutList.add(shout);
    }

    @Lock(READ)
    @Override
    public String[] getAllShouts() {
        System.out.println("NNN: getAllShouts was called");
        return shoutList.toArray(new String[0]);
    }

}
