package day1.beans;

import javax.ejb.Remote;

@Remote
public interface CalculatorBeanRemote {
    public double celToFah(double cel);
}
