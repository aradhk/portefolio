package abbott.ejb;

import javax.ejb.Remote;

@Remote
public interface HelloBeanRemote {

    String sayHello();
    
}
